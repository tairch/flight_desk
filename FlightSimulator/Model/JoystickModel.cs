﻿using FlightSimulator.Model.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightSimulator.Model
{
  public  class JoystickModel : ISenderModel
    {
        public void SendCommand(string path, string value)
        {        
            new Task(() =>
            {
                CommandsNetCustomer.Instance.Send_2(path, value);
            }).Start();
           
        }

        public void SendAutoCommand(string value)
        {
            string[] values = value.Split('\n');
            new Task(() =>
            {
                CommandsNetCustomer.Instance.SendAutoValues(values);
            }).Start();
           
        }
    }
}
   

