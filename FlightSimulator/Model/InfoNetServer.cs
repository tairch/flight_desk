﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightSimulator.Model.Interface;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using FlightSimulator.Model;

namespace FlightSimulator.Model
{

    public class InfoNetServer
    {
        #region Singleton
        private static InfoNetServer m_Instance = null;
        public static InfoNetServer Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new InfoNetServer();
                    m_Instance.stopThreadFlag = false;
                    m_Instance.client = new TcpClient();
                    m_Instance.infoConnected = false;
                    //listener = new TcpListener();
                }
                return m_Instance;
            }
        }
        #endregion
        //private ApplicationSettingsModel applicationSettingsModel;
        //private TcpClient client;
        //private NetworkStream nwStream;

        //private ISettingsModel iSettingsModel;
        //private IFlightInfoModel iFlightInfoModel;
        //private TcpClient client;
        private TcpClient client;
        private IPAddress localAdd;
        private NetworkStream nwStream;
        private static TcpListener listener;
        private string[] settings;
        private bool stopThreadFlag;
        private Thread infoThread;
        private bool infoConnected;

        public bool InfoConnected
        {
            get { return infoConnected; }
        }

        /*public IFlightInfoModel IFlightInfoModel
        {
            get { return iFlightInfoModel; }
            set { iFlightInfoModel = value; }
        }*/

        public void ThreadFunc()
        {
            Thread.CurrentThread.Name = "InfoThread";
            //try to connect a number of times, sleep in between tries
            const int maxRetries = 20;
            const int sleepTime = 1000;
            for (int i = 0; i < maxRetries; i++)
            {
                try
                {
                    Connect();
                    break;
                }
                catch (Exception)
                {
                    Thread.Sleep(sleepTime);
                }
            }
            //loop to get info repeatedly
            do
            {
                byte[] buffer = new byte[client.ReceiveBufferSize];

                //read incoming stream
                int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                //parse the recieved data
                string dataReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                
                ParseDataRecieved(dataReceived);
                UpdateData();
            }
                while (!stopThreadFlag);
                Disconnect();
        }


        //get the info from the flight simulator and update the info in FlightInfo
        public void GetInfo()
        {       
            infoThread = new Thread(ThreadFunc);
            //prevent the thread from keeping the application running after a close
            infoThread.IsBackground = true;
            infoThread.Start();
        }

        private void Connect()
        {
            //const int PORT_NO = applicationSettingsModel.FlightInfoPort;
            const string SERVER_IP = "127.0.0.1";

            //---listen at the specified IP and port no.---
            localAdd = IPAddress.Parse(SERVER_IP);
            
            listener = new TcpListener(localAdd, ApplicationSettingsModel.Instance.FlightInfoPort);
            listener.Start();
            //waiting for client connection
            client = listener.AcceptTcpClient();
            infoConnected = true;
            //client connected
            nwStream = client.GetStream();
        }

        private void ParseDataRecieved(string dataReceived)
        {
            settings = dataReceived.Split(',');

            /*foreach (var set in settings)
            {
                System.Console.WriteLine($"<{set}>");
            }*/
        }

        private void UpdateData()
        {
            if (!(settings == null || settings.Length < 2))
            {
                //double number;
                bool success = double.TryParse(settings[0], out double number);
                if (success)
                {
                    FlightInfo.Instance.Lon = number;
                }
                success = double.TryParse(settings[1], out double numberLat);
                if (success)
                {
                    FlightInfo.Instance.Lat = numberLat;
                }
                //FlightInfo.Instance.Aileron = settings[19];
                //FlightInfo.Instance.Elevator = settings[20];
                //FlightInfo.Instance.Rudder = settings[21];
                //FlightInfo.Instance.Throttle = settings[23];
            }
        }

        private void Disconnect()
        {
            if (client != null)
            {
                client.Close();
            }
            if (listener != null)                                     
            {
                listener.Stop();
            }
            //Console.ReadLine();
        }

        ~InfoNetServer()
        {
            //stop the thread
            stopThreadFlag = true;
            infoThread.Join();
        }
    }
}
