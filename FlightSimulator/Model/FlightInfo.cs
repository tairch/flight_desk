﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightSimulator.Model.Interface;


namespace FlightSimulator.Model
{
    
    class FlightInfo : IFlightInfoModel
    {
        private double lon;
        private double lat;
        /*
        private double aileron;
        private double elevator;
        private double rudder;
        private double throttle;*/

        #region Singleton
        private static FlightInfo m_Instance = null;
        public static FlightInfo Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new FlightInfo();
                    m_Instance.lon = 0;
                    m_Instance.lat = 0;
                }
                return m_Instance;
            }
        }
        #endregion
        public double Lon
        {
            get { return lon; }
            set
            {
                lon = value;
                NotifyPropertyChanged("Lon");
            }
        }

        public double Lat
        {
            get { return lat; }
            set
            {
                lat = value;
                NotifyPropertyChanged("Lat");
            }
        }

        /*
        public double Aileron
        {
            get { return aileron; }
            set { aileron = value; }
        }

        public double Elevator
        {
            get { return elevator; }
            set { elevator = value; }
        }

        public double Rudder
        {
            get { return rudder; }
            set { rudder = value; }
        }

        public double Throttle
        {
            get { return throttle; }
            set { throttle = value; }
        }*/
    }
}
