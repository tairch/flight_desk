﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using FlightSimulator.Model.Interface;
using System.IO;
using System.Threading;

namespace FlightSimulator.Model
{
    public class CommandsNetCustomer
    {
        #region Singleton
        private static CommandsNetCustomer m_Instance = null;
        public static CommandsNetCustomer Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    // ApplicationSettingsModel appSettingsModel= new ApplicationSettingsModel();

                    m_Instance = new CommandsNetCustomer();
                    client = new TcpClient();
                }
                return m_Instance;
            }
        }
        #endregion

        private ApplicationSettingsModel applicationSettingsModel;
        private static TcpClient client;
        private NetworkStream nwStream;

        public ApplicationSettingsModel ApplicationSettingsModel
        {
            get { return applicationSettingsModel; }
            set { applicationSettingsModel = value; }
        }

        public void Connect()
        {
            //---create a TCPClient object at the IP and port no.---
           // client = new TcpClient();
            // //client = new TcpClient(Instance.FlightServerIP, 5402);
            client.Connect(ApplicationSettingsModel.Instance.FlightServerIP, ApplicationSettingsModel.Instance.FlightCommandPort);
           // client.Connect(ApplicationSettingsModel.Instance.FlightServerIP, 5043);
            nwStream = client.GetStream();
        }

        public void Disconnect()
        {
            client.Close();
        }

        public void Send_2(string textToSend, string value)
        {
            //client = new TcpClient();
            //// //client = new TcpClient(Instance.FlightServerIP, 5402);
            //client.Connect(ApplicationSettingsModel.Instance.FlightServerIP, ApplicationSettingsModel.Instance.FlightCommandPort);
            //nwStream = client.GetStream();

            string message = "set" + " " + textToSend + " " + value + "\r\n";
            byte[] msgByte = ASCIIEncoding.ASCII.GetBytes(message);
            nwStream.Write(msgByte,0, msgByte.Length);
       }

        public void SendAutoValues(string[] values)
        {
            foreach(var cmd in values)
            {
                byte[] valByte = ASCIIEncoding.ASCII.GetBytes($"{cmd}\r\n");
                nwStream.Write(valByte, 0, valByte.Length);
                Thread.Sleep(800);
            }
        }


        /*public void Send(string textToSend)
        {
            using (NetworkStream stream = client.GetStream())
            using (BinaryReader reader = new BinaryReader(stream))
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                //---data to send to the server---
                //string textToSend = DateTime.Now.ToString();

                byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(textToSend);

                //---send the text---
                Console.WriteLine("Sending : " + textToSend);
                //writer.write(num);
                nwStream.Write(bytesToSend, 0, bytesToSend.Length);
            }
        }*/
    }
}

