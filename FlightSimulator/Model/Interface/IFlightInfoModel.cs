﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace FlightSimulator.Model.Interface
{
    public abstract class IFlightInfoModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        #endregion

        double Lon { get; set; }
        double Lat { get; set; }
        /*double Aileron { get; set; }
        double Elevator { get; set; }
        double Rudder { get; set; }
        double Throttle { get; set; }*/
    }
}
