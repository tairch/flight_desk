﻿namespace FlightSimulator.Model.Interface
{
    interface ISenderModel
    {
        void SendCommand(string path,string value);
    }
}