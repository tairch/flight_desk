﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FlightSimulator.Model;
using FlightSimulator.ViewModels;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using System.Threading;

namespace FlightSimulator.Views
{
    /// <summary>
    /// Interaction logic for MazeBoard.xaml
    /// </summary>
    public partial class FlightBoard : UserControl
    {
        ObservableDataSource<Point> planeLocations = null;
        private FlightBoardViewModel _vm = new FlightBoardViewModel();
        public FlightBoard()
        {
            InitializeComponent();
            DataContext = _vm;
            _vm.PropertyChanged += Vm_PropertyChanged;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            planeLocations = new ObservableDataSource<Point>();
            // Set identity mapping of point in collection to point on plot

            /*/test
            double[] my_array = new double[10];

            for (int i = 0; i < my_array.Length; i++)
            {
                my_array[i] = Math.Sin(i);
                planeLocations.Collection.Add(new Point(i, my_array[i]));
            }/*///test


            planeLocations.SetXYMapping(p => p);
            Color blue = Color.FromRgb(0,191,255);
            //add the line graph
            plotter.AddLineGraph(planeLocations, blue, 2, "Route");
        }

        private void Vm_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName.Equals("Lat") || e.PropertyName.Equals("Lon"))
            {

                Point p1 = new Point(((FlightBoardViewModel)sender).Lat, ((FlightBoardViewModel)sender).Lon);
                //UI should go through the main thread
                this.Dispatcher.Invoke(() =>
                {
                    planeLocations.Collection.Add(p1);
                });
                planeLocations.AppendAsync(Dispatcher, p1);
            }
        }

    }

}

