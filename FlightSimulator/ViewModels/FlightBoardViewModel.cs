﻿using FlightSimulator.Model;
using FlightSimulator.Model.Interface;
using FlightSimulator.Views.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Threading;

namespace FlightSimulator.ViewModels
{

    public class FlightBoardViewModel : BaseNotify
    {
        private int startedLon;
        private int startedLat;
        public FlightBoardViewModel()
        {
            FlightInfo.Instance.PropertyChanged += Info_PropertyChanged;
            startedLon = 0;
            startedLat = 0;
        }
        private const int wait = 50;
        private double lon;
        public double Lon
        {
            get { return lon; }
            set
            {
                lon = value;
                if (startedLon < wait + 1)
                {
                    startedLon++;
                }
                //notify only if already both started
                if (startedLon > wait && startedLat > wait)
                {
                    NotifyPropertyChanged("Lon");
                }
            }
        }

        private double lat;
        public double Lat
        {
            get { return lat; }
            set
            {
                lat = value;
                if (startedLat < wait + 1)
                {
                    startedLat++;
                }
                //notify only if both already started
                if (startedLon > wait && startedLat > wait)
                {
                    NotifyPropertyChanged("Lat");
                }
            }
        }

        private void Info_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Lat"))
            {
                Lat = ((FlightInfo)sender).Lat;
            }
            if (e.PropertyName.Equals("Lon"))
            {
                Lon = ((FlightInfo)sender).Lon;
            }
        }

        private ICommand _connectCommand;
        public ICommand ConnectCommand
        {
            get
            {
                return _connectCommand ?? (_connectCommand=new CommandHandler(()=>ConnectClick()));
            }
        }
        void ConnectClick()
        {
            try
            {
                InfoNetServer.Instance.GetInfo();

                //try to connect a number of times, sleep in between tries
                const int maxRetries = 20;
                const int sleepTime = 1000;
                for (int i = 0; i < maxRetries; i++)
                {
                    try
                    {
                        CommandsNetCustomer.Instance.Connect();
                        if (InfoNetServer.Instance.InfoConnected)
                        {
                            break;
                        }
                    }
                    catch (Exception)
                    {
                        
                        Thread.Sleep(sleepTime);
                    }
                }

                /*if (InfoNetServer.Instance.InfoConnected)
                {
                    MessageBox.Show("Connection was successfuly established");
                } else
                {
                    MessageBox.Show("Info client connection could not be established");
                    //MessageBox.Show("Please try again");
                }*/
            }
            catch(Exception)
            {
                //MessageBox.Show(ex.ToString());
            }

        }
        private ICommand _settingsCommand;
        public ICommand SettingsCommand
        {
            get
            {
                return _settingsCommand ?? (_settingsCommand = new CommandHandler(() => OnSettingsClick()));
            }
        }
        SettingsWindow settings;
        private void OnSettingsClick()
        {
            //if (!settings.IsLoaded)
            if (settings == null)
            {
                settings = new SettingsWindow();
                settings.Closed += (a, b) => settings = null;
                settings.Show();
            }
            else
                settings.Show();
               
        }
    }
}
