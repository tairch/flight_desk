﻿using FlightSimulator.Model;
using FlightSimulator.Model.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FlightSimulator.ViewModels
{
    public class JoystickViewModel : BaseNotify
    {
        private double aileron;
        public double Aileron
        {
            get { return aileron; }
            set
            {
                aileron = value;
                if (aileron < -1)
                    aileron = -1;
                else if (aileron > 1)
                    aileron = 1;
                //   if (CommandsNetCustomer.Instance.IsConnact)
                NotifyPropertyChanged("Aileron");
                CommandsNetCustomer.Instance.Send_2("/controls/flight/aileron", Convert.ToString(value));
            }
        }
        private double elevator;
        public double Elevator
        {
            get { return elevator; }
            set
            {
                elevator = value;
                if (elevator < -1)
                    elevator = -1;
                else if (elevator > 1)
                    elevator = 1;
                //if (CommandsNetCustomer.Instance.IsConnact)
                CommandsNetCustomer.Instance.Send_2("/controls/flight/elevator", Convert.ToString(value));
                NotifyPropertyChanged("Elevator");
            }
        }

        //Joystick wrapper
        private ISenderModel model;

        public JoystickViewModel()
        {
            model = new JoystickModel();
        }

       

        // private double rudder;
        public double Rudder
        {
            set
            {
                model.SendCommand("/controls/flight/rudder", Convert.ToString(value));
            }
        }

        // private double throttle;
        public double Throttle
        {
            set
            {
                model.SendCommand("/controls/engines/current-engine/throttle", Convert.ToString(value));
            }
        }

        //-------------------------------------------- Auto pilot 
        bool isSent=false;
        private ICommand _clearAutoCommand;
        public ICommand ClearAutoCommand
        {
            get
            {
                return _clearAutoCommand ?? (_clearAutoCommand = new CommandHandler(() => ClearAutoClick()));
            }
        }
        void ClearAutoClick()
        {
            AutoTxt = "";
            NotifyPropertyChanged("BgColor");
            //NotifyPropertyChanged("AutoTxt");
        }

        private ICommand _okAutoCommand;
        public ICommand OKAutoCommand
        {
            get
            {
                return _okAutoCommand ?? (_okAutoCommand = new CommandHandler(() => OKAutoClick()));
            }
        }
        void OKAutoClick()
        {
            if (!String.IsNullOrEmpty(AutoTxt))             
                ((JoystickModel)model).SendAutoCommand(AutoTxt);
            isSent = true;
            NotifyPropertyChanged("BgColor");
        }

        //----
        public string _bgColor;
        public string BgColor
        {
            get
            {            
                return String.IsNullOrEmpty(AutoTxt)|| isSent ? "White": "LightPink";
            }
        }
        private string _autoTxt;

        public string AutoTxt
        {
            get { return _autoTxt; }
            set
            {
                isSent = false;
                _autoTxt = value;
                NotifyPropertyChanged("BgColor");
                NotifyPropertyChanged("AutoTxt");
            }
        }

       
       
        
            
       
    }



}
